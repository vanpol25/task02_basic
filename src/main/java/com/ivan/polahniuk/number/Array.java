package com.ivan.polahniuk.number;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Class Array designed to create a simple sequence.
 * Such as form 1 to n depending of a constructors.
 * Also has constructor for creating sequence
 * with custom input.
 *
 * @author Ivan Polahniuk
 * @version 1.0 beta
 * @since 11.10.2019
 */
public class Array {

    /**
     * Used to set default value of sequent quantity.
     */
    private static final int DEFAULT_SIZE_ARRAY = 100;

    /**
     * It is the main field in class.
     * Consist of integers in list.
     * Creating in any constructors in this class.
     * Also can by created with method {@link Array#numberedArray}.
     */
    private List<Integer> array;

    /**
     * A custom constructor.
     * You can input a quantity of numbers for sequence.
     * Causes another method {@link Array#numberedArray}.
     *
     * @param size - is a quantity of numbers in sequence.
     */
    public Array(int size) {
        numberedArray(size);
    }

    /**
     * Default constructor.
     * Causes another method {@link Array#numberedArray}.
     * Fills in the field {@link Array#array}.
     * Filling occurs form 1 till DEFAULT_SIZE_ARRAY {@link Array#DEFAULT_SIZE_ARRAY}.
     */
    public Array() {
        numberedArray(DEFAULT_SIZE_ARRAY);
    }

    /**
     * Method created to recreate list of numbers.
     * Using size parameter.
     * @param size - is a quantity of numbers in sequence.
     */
    public void createNewArr(int size) {
        numberedArray(size);
    }


    /**
     * This method print ood numbers from first value.
     */
    public void printOddNumbersFromStart() {
        for (int number : getOddNumbers()) {
            System.out.print(number + ", ");
        }
        System.out.println('\n');
    }

    /**
     * This method print even numbers from last value.
     */
    public void printEvenNumbersFromEnd() {
        List<Integer> evenNumbers = getEvenNumbers();
        ListIterator<Integer> li = evenNumbers.listIterator(evenNumbers.size());
        while (li.hasPrevious()) {
            System.out.print(li.previous() + ", ");
        }
        System.out.println('\n');
    }

    /**
     * This method print the sum of ood and even numbers separately.
     */
    public void printSumOfOddAndEvenNumbers() {
        int sumOddNumbers = getSumOfNumbers(getOddNumbers());
        int sumEvenNumbers = getSumOfNumbers(getEvenNumbers());
        System.out.println("Sum of odd numbers: " + sumOddNumbers);
        System.out.println("Sum of even numbers: " + sumEvenNumbers);
    }

    /**
     * This method returns the sum of numbers in list of integers.
     * @param integers - list of integers.
     * @return - the sum of list integers.
     */
    private int getSumOfNumbers(List<Integer> integers) {
        int sum = 0;
        for (Integer number : integers) {
            sum += number;
        }
        return sum;
    }

    /**
     * This method returns all odd numbers from {@link Array#array} as new list.
     * @return - new list of odd numbers.
     */
    private List<Integer> getOddNumbers() {
        List<Integer> oddNumbers = new ArrayList<Integer>();
        for (int number : array) {
            if (number % 2 != 0) {
                oddNumbers.add(number);
            }
        }
        return oddNumbers;
    }

    /**
     * This method returns all even numbers from {@link Array#array} as new list.
     * @return - new list of even numbers.
     */
    private List<Integer> getEvenNumbers() {
        List<Integer> evenNumbers = new ArrayList<Integer>();
        for (int number : array) {
            if (number % 2 == 0) {
                evenNumbers.add(number);
            }
        }
        return evenNumbers;
    }

    /**
     * This method generate list of integers for {@link Array#array}
     * depends of size parameter from 1 till n.
     * @param size - size of generation numbers.
     */
    private void numberedArray(int size) {
        array = new ArrayList<Integer>();
        for (int i = 0; i < size; i++) {
            array.add(i + 1);
        }
    }

}
