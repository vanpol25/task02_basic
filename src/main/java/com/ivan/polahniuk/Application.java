package com.ivan.polahniuk;

import com.ivan.polahniuk.fibonacci.Fibonacci;
import com.ivan.polahniuk.number.Array;

/**
 * Class Application with entry point main().
 *
 * @author Ivan Polahniuk
 * @version 1.0 beta
 * @since 11.10.2019
 */
public class Application {
    /**
     * The entry point of program.
     * Include objects of classes and their implementations.
     */
    public static void main(String[] args) {
        Array array = new Array();
        array.printOddNumbersFromStart();
        array.printEvenNumbersFromEnd();
        array.printSumOfOddAndEvenNumbers();

        Fibonacci f = new Fibonacci();
        System.out.println("The biggest odd number: "
                + f.getTheBiggestOddNumber());
        System.out.println("The biggest even number: "
                + f.getTheBiggestEvenNumber());
        f.printPercentageOfOddAndEvenNumbers();
    }
}
