package com.ivan.polahniuk.fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Fibonacci designed to create a Fibonacci sequence.
 * Also has constructor for creating sequence
 * with custom input.
 *
 * @author Ivan Polahniuk
 * @version 1.0 beta
 * @since 11.10.2019
 */
public class Fibonacci {

    /**
     * Used to calculate the percentage value returned by methods.
     */
    private static final int ONE_HUNDRED_PERCENT = 100;

    /**
     * Used to set default value of sequent quantity.
     */
    private static final int DEFAULT_SIZE = 10;

    /**
     * It is the main field in class.
     * Consist of integers in list.
     * Creating in any constructors in this class.
     * Also can by created with method {@link Fibonacci#createFibonacci}.
     */
    private List<Integer> listFibonacci = new ArrayList<Integer>();


    /**
     * A custom constructor.
     * You can input a quantity of numbers for sequence.
     * Causes another method {@link Fibonacci#createFibonacci}.
     *
     * @param size - is a quantity of numbers in sequence.
     */
    public Fibonacci(int size) {
        createFibonacci(size);
    }

    /**
     * Default constructor.
     * Causes another method {@link Fibonacci#createFibonacci}.
     * Fills in the field {@link Fibonacci#listFibonacci}
     * with fibonacci algorithm.
     */
    public Fibonacci() {
        createFibonacci(DEFAULT_SIZE);
    }

    /**
     * Fills in the field {@link Fibonacci#listFibonacci}
     * with fibonacci algorithm.
     * Also can print this sequence.
     *
     * @param size - is a quantity of numbers in sequence.
     */
    private void createFibonacci(int size) {
        for (int i = 0; i < size; i++) {
            listFibonacci.add(fibonacci(i));
        }
        print();
    }

    /**
     * Is a recursive method.
     * Used to generate Fibonacci numbers
     * in method {@link Fibonacci#createFibonacci(int)}.
     *
     * @param n - is a input value of recursive method.
     * @return return the next value of Fibonacci sequence.
     */
    private int fibonacci(int n) {
        int number;
        if (n == 0) {
            number = 0;
        } else if (n == 1) {
            number = 1;
        } else {
            number = fibonacci(n - 1) + fibonacci(n - 2);
        }
        return number;
    }


    /**
     * Method find the biggest odd number.
     * Its used foreach and operator "if".
     *
     * @return - the biggest odd number.
     */
    public int getTheBiggestOddNumber() {
        int biggestOddNumber = 0;
        for (Integer num : listFibonacci) {
            if (num % 2 != 0 && biggestOddNumber < num) {
                biggestOddNumber = num;
            }
        }
        return biggestOddNumber;
    }

    /**
     * Method find the biggest even number.
     * Its used foreach and operator "if".
     *
     * @return - the biggest even number.
     */
    public int getTheBiggestEvenNumber() {
        int biggestEvenNumber = 0;
        for (Integer num : listFibonacci) {
            if (num % 2 == 0 && biggestEvenNumber < num) {
                biggestEvenNumber = num;
            }
        }
        return biggestEvenNumber;
    }

    /**
     * Method prints all before created Fibonacci sequence.
     */
    private void print() {
        for (Integer i : listFibonacci) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }

    /**
     * Method print in console percentage of Odds and Evens numbers.
     * Used counters to which counts in foreach.
     * Then print it in console with the calculated data.
     */
    public void printPercentageOfOddAndEvenNumbers() {
        double countOddNumbers = 0;
        double countEvenNumbers = 0;
        for (Integer num : listFibonacci) {
            if (num % 2 != 0) {
                countOddNumbers++;
            } else {
                countEvenNumbers++;
            }
        }
        double percentageOfOdds = (countOddNumbers / listFibonacci.size())
                * ONE_HUNDRED_PERCENT;
        double percentageOfEvens = (countEvenNumbers / listFibonacci.size())
                * ONE_HUNDRED_PERCENT;
        System.out.println("There is Odd/Even numbers like: "
                + percentageOfOdds + '/' + percentageOfEvens);
    }

}
